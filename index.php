<?php
$p = $_GET['p'];
$i = $_GET['i'];
$x = $_GET['x'];
$e = $_GET['e'];
$l = $_GET['l'];

session_start();
$protocol = 'http://'; 
$host = $_SERVER['HTTP_HOST'];
include "services/config.php";


// Homepage
if(!$p){
	include "partials/head.php";
	include "pages/home.php";
	include "partials/footer.php";

// Signup
} else if ($p=='signup'){
	include "partials/head.php";
	include "pages/signup.php";
	include "partials/footer.php";

// Login
} else if ($p=='login'){
	include "partials/head.php";
	include "pages/login.php";
	include "partials/footer.php";

// Admin Events
} else if ($p=='events' && !$i){
	include "services/auth.php";
	include "partials/head.php";
	include "partials/admin.php";
	include "pages/events.php";
	include "partials/footer.php";
	
// Admin Events
} else if ($p=='events' && $i){
	include "services/auth.php";
	include "partials/head.php";
	include "partials/admin.php";
	include "pages/event_details.php";
	include "partials/footer.php";

// Admin Account
} else if ($p=='account'){
	include "services/auth.php";
	include "partials/head.php";
	include "partials/admin.php";
	include "pages/account.php";
	include "partials/footer.php";

// Admin Account
} else if ($p=='attendees'){
	include "services/auth.php";
	include "partials/head.php";
	include "partials/admin.php";
	include "pages/attendees.php";
	include "partials/footer.php";

// Public Event
} else if($p=='event' && $i)  {
	include "partials/head.php";
	include "pages/event.php";
	include "partials/footer.php";
}


?>