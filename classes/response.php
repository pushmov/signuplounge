<?php

class Response {

	const DEFAULT_ERROR = 'An error occurred. Please try again later.';
	
	public function output($message = null){
		if ($message === null)
			$message = self::DEFAULT_ERROR;

		die($message);
		exit();
	}
}