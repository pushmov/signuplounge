<html ng-app="signupLounge">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114157789-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-114157789-1');
</script>
<base href="<?php echo $protocol . $host?>">
<meta name="viewport" content="initial-scale=1.0">
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,400" rel="stylesheet">
<link rel="stylesheet" href="css/style.1.5.css" type="text/css">
<script src="js/global.1.2.js"></script>
</head>
<body ng-controller="signupLoungeCtrl">
<div id="wrapper">
<div id="inner-wrapper">
	<header id="main">
    	<a id="logo" href="/"></a>
        <nav>
        	<?php if($member_id){?>
        		<a href="account"><?=$member['email']?></a>
        		<a href="services/logout.php">logout</a>
            <? } else {?>
        		<a class="signup" href="signup">Create an Account</a>
        		<a href="login">Sign in</a>
			<? } ?>	
        </nav>
        <div class="clr"></div>
    </header>