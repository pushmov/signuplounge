<header id="admin">
	<nav>
        <a class="<?=$p=='events'?'cur':''?>" href="events"><span class="icon calendar"></span>Your Events</a>
        <a class="<?=$p=='attendees'?'cur':''?>" href="attendees"><span class="icon group"></span>Attendees</a>
        <a class="<?=$p=='account'?'cur':''?>" href="account"><span class="icon cogwheel"></span>Account Settings</a>
    </nav>
</header>
