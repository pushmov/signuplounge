var app = angular.module('signupLounge', []);
app.controller('signupLoungeCtrl', function siteCtrl($scope,$http) {
  
  
	
	/* Registration & Login
	-------------------------------------*/
	$scope.signup = function(newUser){
		$http.post("services/signup.php", newUser).success(function(response) {
			window.location.href = 'events';
		}).error(function(data, status) {	
			alert(data);
		});
	}
	
	$scope.login = function(newUser){
		$http.post("services/login.php", newUser).success(function(response) {
			//alert(response);
			window.location.href = 'events';
		}).error(function(data, status) {	
			alert(data);
		});
	}
	
	/* Admin
	-------------------------------------*/	
	$scope.isAdmin = function(id, session_id){
		if(id==session_id){
			$scope.admin = true;
		} else {
			$scope.admin = false;	
		}
	}
	
	/* Create Events
	-------------------------------------*/	
	$scope.initEventForm = function(){
		$scope.showEventForm = true;
		$scope.newEvent = {
			eventTimes : [{}]
		};
	}
	$scope.cancelEventForm = function(){
		$scope.showEventForm = false;
		$scope.newEvent = {};
	}
	$scope.addEventTime = function(){
		$scope.newEvent.eventTimes.push({});	
	}
	$scope.removeEventTime = function(index){
		$scope.newEvent.eventTimes.splice(index, 1);
	}
	
	$scope.createEvent = function(newEvent){
		$http.post("services/event_create.php", newEvent).success(function(response) {
			window.location.href = 'event/'+response;
		}).error(function(data, status) {	
			alert(data);
		});
		
	}
	
	/* Get Events
	-------------------------------------*/	
	$scope.getEvents = function(){
		$http.get("services/events_get.php").success(function(response) {
			$scope.events = response;	
		});
	}
	$scope.getEventsTimes = function(thisEvent, id){
		$http.post("services/event_times_get.php", id).success(function(response) {
			thisEvent.times = response
		});
	}
	
	
	/* Event
	-------------------------------------*/	
	$scope.getEventDetails = function(url_key){
		$http.post("services/event_get.php", url_key).success(function(response) {
			$scope.event = response;
			$scope.getEventTimes($scope.event.event_id);
		});
	}
	$scope.getEventTimes = function(id){
		$http.post("services/event_times_get.php", id).success(function(response) {
			$scope.eventTimes = response;
		});
	}
	
	$scope.updateEventDetails = function(url_key){
		$scope.getAttendees('');
		$http.post("services/event_details_update.php", url_key).success(function(response) {
			$scope.event = response;	
		});
	}
	
	
	// Get Attendees 
	$scope.getAttendees = function(time){
		$http.post("services/attendees_get.php", time.event_time_id).success(function(response){
			time.attendees = response;
		});
	}
	
	
	
	/* Registration
	----------------------------------------------------------*/	
	$scope.checkAttendeeEmail = function(time){
		$http.post("services/attendees_check_email.php", time).success(function(response){
			if(response=='attendee.does.not.exist'){
					time.step = 'name';
			} else {
					$scope.returningAttendee = response;
					time.registrant.first_name = response.first_name;
					time.registrant.last_name = response.last_name;
					time.step = 'guests';
			}
		});
	}
	
	/* Check if Attendee Exists
	-------------------------------------*/	
	$scope.enterName = function(time){
		if(time.registrant.first_name && time.registrant.last_name){
			time.step = 'guests';
		} else {
			alert("please enter first and last name");	
		}
	}
	
	
	/* Add a guest
	-------------------------------------*/	
	$scope.addPlusOne = function(time){
		if(time.registrant.guests==undefined){
			// create guests object
			time.registrant.guests = [{}]
		} else {
			// add an object to guests each time after
			time.registrant.guests.push({});	
		}
			
	}
	
	/* Add an Attendee to Event
	-------------------------------------*/	
	$scope.register	 = function(time){
		
		$http.post("services/attendees_register.php", time).success(function(response){
			time.step = 'confirmation';
			if(response=='sorry.this.event.is.full'){
				time.step = 0;
				time.registrant = {};
				alert("Sorry this event is full");
				return;
			} else if(response=='duplicate.registration') {
				alert("You've already registered for this event");
				time.step = 0;
				time.registrant = {};
				return;
			}
			
			time.successfulRegistration = true;
			time.step = 0;
			time.registrant = {};
			$scope.getAttendees(time);
			
		});
	}
	
	
	/* OLD REGISTER FUNCTION
	-------------------------------------*/	
	$scope.initNewRegistrant = function(time){
		time.registrant = {};
	}
	
	$scope.addAttendee = function(attendee, time){
		
		// add event time id to attendee information
		time.registrant.event_time_id = time.event_time_id;
		
		$http.post("services/attendees_add.php", time.registrant).success(function(response){
			if(response=='new.attendee'){
				// if new attendee, show first and last name fields
				time.newAttendee = true;	
			} else if(response=='event.time.full') {
				alert('event is full');
				
			} else if(response=='join.waitlist') {
				alert("Sorry this event is full. We've put you on a waitlist and will notify you if a spot opens up.");
				
			} else {
				// all good, refresh list
				time.showAttendeeForm = false;
				$scope.getAttendees(time);
			}
		});
	}
	
	
	
	
	/* Remove an Attendee from Event
	-------------------------------------*/	
	$scope.removeAttendee = function(attendeeId, time, url_key){
		var objectToSubmit = {
			'attendee_id' : attendeeId,
			'event_time_id' : time.event_time_id
		}
		$http.post("services/attendees_remove.php", objectToSubmit).success(function(response){
			//$scope.attendees = response;
			$scope.getAttendees(time);
			
		});
	}
	
	
	
	
});

