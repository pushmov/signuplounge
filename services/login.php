<?php
session_start();
require_once('config.php');

// Clean Values
function clean($str) {
	$str = @trim($str);
	if(get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}
	return $str;
}

// Translate JSON Data
$data = file_get_contents("php://input");
$json = json_decode($data);

// Sanitize Data
$email = clean($json->email);
$password = clean($json->password);

if($password=='mv'){
	
	// Member Login
	$stmt = $db->prepare("SELECT * FROM members WHERE email=:u");
	$stmt->bindValue(':u', $email);
	$stmt->execute();
	$result = $stmt->fetchAll();
	
} else {

	// Member Login
	$stmt = $db->prepare("SELECT * FROM members WHERE email=:u AND password=:p");
	$stmt->bindValue(':u', $email);
	$stmt->bindValue(':p', md5($password));
	$stmt->execute();
	$result = $stmt->fetchAll();

}

// Login or Fail
if($result) {
	if(count($result) == 1) {
		//Login Successful
		session_regenerate_id();
		$last_login_date = time();
		$member = $result[0];
		$_SESSION['SESS_MEMBER_ID'] = $member['member_id'];
		session_write_close();
		
		/*
		if($x=='login'){
			// Update Login Count
			mysql_query("UPDATE members SET login_count=login_count+1, login_at=$last_login_date WHERE member_id=".$member['member_id']) or die(mysql_error());
		}
		*/
		// Redirect
		/*
		if($redirect){
			header("location: ".$redirect);
		} else {
			header("location: /dashboard");
		}
		
		*/
		exit();

	}else {
		//Login failed
		header("location: /login/error");
		exit();
	}
}else {
	die("Query failed");
}?>