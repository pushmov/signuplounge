<?php
session_start();
require_once('config.php');

// Clean Values
function clean($str) {
	$str = @trim($str);
	if(get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}
	return $str;
}

// Change Names to Url Friendly Names
function to_slug($string){
	return str_replace('---','-', strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string))));
}


// Decode Json Data
$data = file_get_contents("php://input");
$json = json_decode($data);

// Grab Form Data
$first_name = clean($json->first_name);
$last_name = clean($json->last_name);
$email = clean($json->email);
$event_time_id = clean($json->event_time_id);


/* New user or existing user 
----------------------------------------*/

// Does attendee email exist?
$stmt = $db->prepare("SELECT * FROM attendees WHERE email=:u AND first_name != '' LIMIT 1");
$stmt->bindValue(':u', $email);
$stmt->execute();
$existing_attendee = $stmt->fetchAll();

// No, ask for name
if(count($existing_attendee)==0 && !$first_name){
	
	// Return error code
	exit("new.attendee");
	
// No, Enter New Attendee
} else if (count($existing_attendee)==0 && $first_name){
	// Insert New Attendee
	$result = $db->prepare("INSERT INTO attendees (first_name, last_name, email, date_created) VALUES(:first_name, :last_name, :email, :date_created)");
	$result->bindValue(':first_name', $first_name);
	$result->bindValue(':last_name', $last_name);
	$result->bindValue(':email', $email);
	$result->bindValue(':date_created', time());
	$exec = $result->execute();
	// Get New Attendee Id
	$attendee_id = $db->lastInsertId();
	
// Yes, use their ID	
} else {
	$attendee_id = $existing_attendee[0]['attendee_id'];
}



/* Check Capacity 
----------------------------------------*/


//count current attendees 
$stmt = $db->prepare("SELECT * FROM rsvps WHERE event_time_id=:u");
$stmt->bindValue(':u', $event_time_id);
$stmt->execute();
$rsvps = $stmt->fetchAll();

// Check to make sure we don't exceed event time capacity
$stmt = $db->prepare("SELECT * FROM event_times WHERE event_time_id=:u");
$stmt->bindValue(':u', $event_time_id);
$stmt->execute();
$event_time = $stmt->fetch(PDO::FETCH_ASSOC);

// if Event is full
if(count($rsvps) >= $event_time['capacity']){
	if(!$event_time['waitlist']){
		
		// Sorry, event is full
		exit('event.time.full');	
		
	} else {
		
		// If waitlist options is available for this event - set this to true
		$add_to_waitlist = true;	
	}
}



/* Confirm RSVP
----------------------------------------*/

// Insert RSVP
$result = $db->prepare("INSERT INTO rsvps (attendee_id, event_time_id, date_created) VALUES(:attendee_id, :event_time_id, :date_created)");
$result->bindValue(':attendee_id', $attendee_id);
$result->bindValue(':event_time_id', $event_time_id);
//$result->bindValue(':w', $add_to_waitlist);
$result->bindValue(':date_created', time());
$exec = $result->execute();


// Get Event Info Via Event_Time_Id
$get_event = $db->prepare("SELECT * FROM events
JOIN event_times ON (event_times.event_id = events.event_id)
JOIN rsvps ON (rsvps.event_time_id = event_times.event_time_id)
WHERE rsvps.event_time_id = :event_time_id;");
$get_event->bindValue(':event_time_id', $event_time_id);
$get_event->execute();
$event = $get_event->fetch(PDO::FETCH_ASSOC);




if($add_to_waitlist) {
		
	// Email Administrators
	$to = $email ;
	$subject = "You've been added to waitlist!";
	$message = "Hello!<br><br>";
	$message .= "You've been added to a waitlist for an event. We'll notify you if someone cancels. For more information on this event, go here: http://signuplounge.com/event/".$event['url_key']." <br><br>";
	$message .= "Best,<br>";
	$message .= "SignupLounge Team<br>www.signuplounge.com";
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= "From:  <info@signuplounge.com>";
	mail($to,$subject,$message,$headers);
	
	
} else {

	
	
	// Email Administrators
	$to = $email ;
	$subject = "You joined an event!";
	$message = "Hello!<br><br>";
	$message .= "You've signed up for an event! For more information on this event, go here: http://signuplounge.com/event/".$event['url_key']." <br><br>";
	$message .= "Best,<br>";
	$message .= "SignupLounge Team<br>www.signuplounge.com";
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= "From:  <info@signuplounge.com>";
	mail($to,$subject,$message,$headers);



}

// Return Key For attendee list updater
exit($event_time_id);




?>