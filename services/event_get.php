<?php
session_start();
require_once('config.php');


// Decode Json Data
$data = file_get_contents("php://input");
$json = json_decode($data);

// Clean Values
function clean($str) {
	$str = @trim($str);
	if(get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}
	return $str;
}


// Get Projects
$get_projects = $db->prepare("SELECT *, DATE_FORMAT(date, '%W %b %d, %Y') AS date FROM events WHERE url_key=:url_key ORDER BY date DESC");
$get_projects->bindValue(':url_key', clean($data));
$get_projects->execute();


// Returns valid json object - but can't format retrieved dates here :(
$results=$get_projects->fetch(PDO::FETCH_ASSOC);
$json=json_encode($results);
print($json);

?>