<?php 
session_start();
require_once('config.php');

// Clean Values
function clean($str) {
	$str = @trim($str);
	if(get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}
	return $str;
}

// Translate JSON Data
$data = file_get_contents("php://input");
$json = json_decode($data);

// Sanitize Data
$email = clean($json->email);
$password = clean($json->password);

// Insert 
$result = $db->prepare("INSERT INTO members (email, password, date_created) VALUES(:email, :pass, :time)");
$result->bindValue(':email', $email);
$result->bindValue(':pass', md5($password));
$result->bindValue(':time', time());
$exec = $result->execute();
	
// Check whether the query was successful or not
if($exec) {
	
	//Create query
	$stmt = $db->prepare("SELECT * FROM members WHERE email=:u AND password=:p");
	$stmt->bindValue(':u', $email);
	$stmt->bindValue(':p', md5($password));
	$stmt->execute();
	$result = $stmt->fetchAll();
	
	if(count($result) == 1) {
		
		// Login
		session_regenerate_id();
		$member = $result[0];
		$_SESSION['SESS_MEMBER_ID'] = $member['member_id'];
		session_write_close();
		
		// Go to Welcome
		header("location: /admin/welcome");
		
		// Email Administrators
		$to = $member['email'];
		$subject = "Welcome to SignupLounge!";
		$message = "Welcome to SignupLounge!<br><br>";
		$message .= "Thank you for trying out our service. Your events will now be easy to manage. Enjoy!<br><br>";
		$message .= "Best,<br>";
		$message .= "Matt Visk <br>Portfoliolounge, Founder";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= "From: Matt Visk <info@signuplounge.com>";
		mail($to,$subject,$message,$headers);
		exit();
		
	}
	
} else {
	
	die ("We're sorry, the email address you chose is already in use.");
	
}

?>
