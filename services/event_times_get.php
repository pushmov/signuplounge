<?php
session_start();
require_once('config.php');

// Decode Json Data
$data = file_get_contents("php://input");
$json = json_decode($data);

// Clean Values
function clean($str) {
	$str = @trim($str);
	if(get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}
	return $str;
}


// Get Projects
$get_projects = $db->prepare("SELECT *, DATE_FORMAT(start_time, '%l:%i%p') AS start_time , DATE_FORMAT(end_time, '%l:%i%p') AS end_time FROM event_times WHERE event_id=:event_id ORDER BY start_time");
$get_projects->bindValue(':event_id', clean($data));
$get_projects->execute();


// Returns valid json object - but can't format retrieved dates here :(
$results=$get_projects->fetchAll(PDO::FETCH_ASSOC);
$json=json_encode($results);
print($json);

?>