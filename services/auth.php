<?php

//Start session
session_start();

// Get Redirect Value
$redirect = $_SERVER[REQUEST_URI];
$_SESSION['LOGIN_REDIRECT'] = $redirect;

// Check if User is Logged In
if(!isset($_SESSION['SESS_MEMBER_ID']) || (trim($_SESSION['SESS_MEMBER_ID']) == '')) {
	
	// Not Logged In -> Redirect to Login
	header("location: /login");
	exit();
	
	
}
/* else {
	
	// Logged In -> Grab Member Data
	include_once 'config.php';
	$result = $db->prepare("SELECT * FROM members WHERE member_id=:memberid LIMIT 1");
	$result->bindValue(':memberid', $_SESSION['SESS_MEMBER_ID']);
	$result->execute();
	$member = $result->fetch();
	$member_id = $member['member_id'];


} */
?>