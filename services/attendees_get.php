<?php
session_start();
require_once('config.php');


// Decode Json Data
$data = file_get_contents("php://input");
$json = json_decode($data);

$event_time_id = clean($data);

// Clean Values
function clean($str) {
	$str = @trim($str);
	if(get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}
	return $str;
}

// Get Projects
$get_projects = $db->prepare("SELECT attendees.*
FROM attendees
JOIN rsvps ON attendees.attendee_id=rsvps.attendee_id
WHERE rsvps.event_time_id = :event_time_id AND rsvps.waitlisted!=1
ORDER BY rsvps.rsvp_id");
$get_projects->bindValue(':event_time_id', $event_time_id);
$get_projects->execute();


// Returns valid json object
$results=$get_projects->fetchAll(PDO::FETCH_ASSOC);
$json=json_encode($results);
print($json);

?>