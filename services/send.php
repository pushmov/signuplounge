<?php

$data = file_get_contents("php://input");
$json = json_decode($data);

$customer['name'] = $json->name;
$customer['email'] = $json->email;
$customer['estimate'] = $json->estimate;
$customer['effort'] = $json->effort;
$customer['dishes'] = $json->dishes;
$customer['laundry'] = $json->laundry;
$customer['organize'] = $json->organize;
$customer['size'] = $json->size;
$customer['state'] = $json->state;
$customer['city'] = $json->city;
$customer['eco'] = $json->eco;




// Message
$message = "<b>Customer Quote.</b>";
$message .= "<br>";
$message .= "Name: ".$customer['name']."<br>";
$message .= "Email: ".$customer['email']."<br>";
$message .= "Effort: ".$customer['effort']."<br>";
$message .= "Home Size: ".$customer['size']."<br>";
$message .= "Estimate: $".$customer['estimate']."<br>";
$message .= "<br>";
$message .= "<b>Extras</b>";
$message .= "<br>";
$message .= "Dishes: ".$customer['dishes']."<br>";
$message .= "Laundry: ".$customer['laundry']."<br>";
$message .= "Organize: ".$customer['organize']."<br>";
$message .= "<br>";
$message .= "<b>Location</b>";
$message .= "<br>";
$message .= "State: ".$customer['state']."<br>";
$message .= "City: ".$customer['city']."<br>";
$message .= "Eco: ".$customer['eco']."<br>";

// Subject
$subject = "KristyClean.com quote for ".$customer['name'];

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= 'From: <'.$customer['email'].'>' . "\r\n";
$headers .= 'Cc: mattvisk@gmail.com';
$to = 'kre498@yahoo.com';
//$to = 'mattvisk@gmail.com';
$from = $customer['email'];

mail($to,$subject,$message,$headers);



?>
