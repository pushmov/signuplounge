<?php
session_start();
require_once('config.php');

// Clean Values
function clean($str) {
	$str = @trim($str);
	if(get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}
	return $str;
}

// Decode Json Data
$data = file_get_contents("php://input");
$json = json_decode($data);

// Grab Form Data
$attendee_id = clean($json->attendee_id);
$event_time_id = clean($json->event_time_id);

// Delete Attendee's RSVP
$delete = $db->prepare("DELETE FROM rsvps WHERE event_time_id = :event_time_id AND attendee_id = :attendee_id");
$delete->bindValue(':event_time_id', $event_time_id);
$delete->bindValue(':attendee_id', $attendee_id);
$delete->execute();


// Get Attendee Info
$get_projects = $db->prepare("SELECT * FROM attendees WHERE attendee_id=:attendee_id");
$get_projects->bindValue(':attendee_id', $attendee_id);
$get_projects->execute();
$attendee = $get_projects->fetch(PDO::FETCH_ASSOC);


// Get Event Info Via Event_Time_Id
$get_event = $db->prepare("SELECT * FROM events
JOIN event_times ON (event_times.event_id = events.event_id)
JOIN rsvps ON (rsvps.event_time_id = event_times.event_time_id)
WHERE rsvps.event_time_id = :event_time_id;");
$get_event->bindValue(':event_time_id', $event_time_id);
$get_event->execute();
$event = $get_event->fetch(PDO::FETCH_ASSOC);


// Email Administrators
$to = $attendee['email'];
$subject = "You are no longer signed up for an event.";
$message = "Hello!<br><br>";
$message .= "You are no longer signed up for an event! For more information on this event, go here: http://signuplounge.com/event/".$event['url_key']." <br><br>";
$message .= "Best,<br>";
$message .= "SignupLounge Team<br>www.signuplounge.com";
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "From:  <info@signuplounge.com>";
mail($to,$subject,$message,$headers);


?>