<?php
session_start();
require_once('config.php');
require_once('auth.php');

// Clean Values
function clean($str) {
	$str = @trim($str);
	if(get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}
	return $str;
}

// Change Names to Url Friendly Names
function to_slug($string){
	return str_replace('---','-', strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string))));
}

// Check Authentication
if($member_id){
	
	// Decode Json Data
	$data = file_get_contents("php://input");
	$json = json_decode($data);
	
	// Grab Form Data
	$name = clean($json->name);
	$description = clean($json->description);
	$location = clean($json->location);
	$slug = to_slug($name);
	$url_key = substr(md5(microtime().rand()),0,6);
	$date = clean($json->eventDate);
	$date = date('Y-m-d', strtotime($date));
	$times = $json->eventTimes;

	// Insert Data
	$result = $db->prepare("INSERT INTO events (member_id, name, slug, date, description, location, url_key, date_created) VALUES(:member_id, :name, :slug, :date, :description, :location, :url_key, :date_created)");
	$result->bindValue(':member_id', $member_id);
	$result->bindValue(':name', $name);
	$result->bindValue(':date', $date);
	$result->bindValue(':slug', $slug);
	$result->bindValue(':description', $description);
	$result->bindValue(':location', $location);
	$result->bindValue(':url_key', $url_key);
	$result->bindValue(':date_created', time());
	$exec = $result->execute();
	
	// Get new event ID
	$event_id = $db->lastInsertId();
	
	// Insert each event time
	foreach($times as $time) {
		
		$start_time = date("H:i", strtotime(clean($time->startTime)));
		$end_time = date("H:i", strtotime(clean($time->endTime)));
		
		$result = $db->prepare("INSERT INTO event_times (event_id, start_time, end_time, capacity, waitlist) VALUES(:event_id, :start_time, :end_time, :capacity, :waitlist)");
		$result->bindValue(':event_id', $event_id);
		$result->bindValue(':start_time',$start_time);
		$result->bindValue(':end_time', $end_time);
		$result->bindValue(':capacity', clean($time->capacity));
		$result->bindValue(':waitlist', clean($time->waitlist));
		$exec = $result->execute();
	}
	
	exit($url_key);
	
	
// Catch Unauthenticated Users
} else {
	exit('not authenticated');	
}?>