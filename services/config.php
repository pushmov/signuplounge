<?php

$site_url = $_SERVER['HTTP_HOST'];	
date_default_timezone_set("UTC");

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if ($_SERVER['SERVER_NAME'] == 'signuplounge.dev') {

	define('ENV', 'DEVELOPMENT');
	define('SITE_URL', 'signuplounge.dev');
	define('DB_HOST', 'localhost');
	define('DB_USER', 'root');
	define('DB_PASSWORD', 'root1');
	define('DB_DATABASE', 'mattvisk_signuplounge');

} else {

	define('ENV', 'PRODUCTION');
	define('SITE_URL', 'signuplounge.com');
	define('DB_HOST', 'localhost');
	define('DB_USER', 'mattvisk_sula');
	define('DB_PASSWORD', 'vf3G}4OS5J7M');
	define('DB_DATABASE', 'mattvisk_signuplounge');
}



$db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_DATABASE.';charset=utf8mb4', DB_USER, DB_PASSWORD);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	
	
if(isset($_SESSION['SESS_MEMBER_ID']) && $_SESSION['SESS_MEMBER_ID']){
	$result = $db->prepare("SELECT * FROM members WHERE member_id=:memberid LIMIT 1");
	$result->bindValue(':memberid', $_SESSION['SESS_MEMBER_ID']);
	$result->execute();
	$member = $result->fetch();
	$member_id = $member['member_id'];
} else {
	$member_id = null;
}

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/response.php');
$response = new Response();
	
?>