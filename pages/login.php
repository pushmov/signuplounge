<div class="container">
    <div class="title-block">
    	<h1>Login</h1>
        <h2>Welcome back!</h2>
    </div>
    <form name="userForm" ng-submit="login(user)">
        <div class="nice">
            <label>Email</label>
            <input name="email" ng-model="user.email">
            <label>Password</label>
            <input type="password" name="password" ng-model="user.password">
            <!-- redirect -->
            <input type="hidden" ng-model="user.password">
        </div>
        <input class="btn" type="submit" value="Login">
    </form>
</div>