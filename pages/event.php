<title ng-if="event.name" ng-init="isAdmin(event.member_id, <?=$member_id?>)" ng-bind="event.name + ' | Signup'">Sign up here &#x270f</title>


<!-- Event -->
<div class="event-wrapper" ng-init="getEventDetails('<?=$i?>')" ng-show="eventTimes && event">

	<div class="title">
        <h1>{{event.name}}</h1>
        <p class="date">{{event.date}}</p>
        <p>{{event.location}}</p>
    </div>
    <p class="description">{{event.description}}</p>
    
    
    
    
    
    <!--------------------------- Repeat for each timeslot --------------------------->
    <div class="time-wrapper" ng-repeat="time in eventTimes" ng-init="getAttendees(time)">
    	
        <!-- Time Info -->
    	<div class="time-title">
            <a class="right btn" ng-show="!time.step" ng-click="time.step=1">+ Join</a>
            <time>{{time.start_time}} - {{time.end_time}}</time>
            <p ng-show="time.capacity">{{time.attendees.length}} of {{time.capacity}} spots filled</p>
            <p ng-show="!time.capacity">{{time.attendees.length}} going so far</p>
            <div class="clr"></div>
        </div>
        
        <div ng-show="time.successfulRegistration" class="success">
        	<a class="exit right" ng-click="time.successfulRegistration=0">x</a>
        	<h2>You did it!</h2>
            <p>See you there!</p>
        </div>
        
        <!-- ATTENDEES -->
        <ul class="attendee-list" ng-show="!time.step && time.attendees">
            <li ng-repeat="attendee in time.attendees">
                <a href=""><span style="opacity:.25;font-size:16px;" ng-show="attendee.parent_id">&rdsh;</span> {{attendee.first_name}} {{attendee.last_name}}</a>
                <a ng-show="admin" ng-click="removeAttendee(attendee.attendee_id, time)" class="redtext">Remove</a>
            </li>
        </ul>
       
        
        
        <!-------------------------- Registration! ----------------------->
        <div class="registration" ng-show="time.step">
            
            <form ng-submit="checkAttendeeEmail(time)" ng-show="time.step==1">
                <!-- Attendee enters their email -->
                <div class="nice">
                    <label>Ok, what is your email?</label>
                    <input name="email" placeholder="email" ng-model="time.registrant.email">
                </div>
                <input type="submit" class="btn" value="Next &raquo;">
                <!--
                <a class="cancel" ng-click="time.showAttendeeForm=0">Cancel</a>
                -->
                
            </form>
            
            
            <!-- Are you bringing guests? -->
            <div ng-if="time.step=='name'">
                <form ng-submit="enterName(time)">
                    <h3>What's your name?</h3>
                    <div class="nice">
                        <label>First Name</label>
                        <input ng-model="time.registrant.first_name">
                        <label>Last Name</label>
                        <input ng-model="time.registrant.last_name">
                    </div>
                    <input type="submit" class="btn" value="Next &raquo;">
                </form>
            </div>
            
            
            <!-- Are you bringing guests? -->
            <div ng-show="time.step=='guests'">
                <p ng-show="returningAttendee">Hey {{returningAttendee.first_name}}! Welcome back.</p>
                <h3>Are you bringing anyone?</h3>
                <a class="btn" ng-click="addPlusOne(time);time.step=2">Yes</a>
                <a class="btn" ng-click="time.step=3">No</a>
            </div>
            
            
            <!-- How many Guests? What are their names? -->
            <div ng-show="time.step==2">
                <div ng-repeat="guest in time.registrant.guests">
                    <form>
                    	<div class="nice">
                            <label>Guest's First Name</label>
                            <input ng-model="guest.first_name">
                            <label>Guest's Last Name</label>
                            <input ng-model="guest.last_name">
                        </div>
                    </form>
                </div>
                <a class="cancel" ng-click="addPlusOne(time)">+ Add Another Guest</a>
                <hr />
                <a class="btn" ng-click="time.step=3">Next</a>
            </div>
            
            
            
            <!-- Confirmation -->
            <div ng-show="time.step==3">
                <ul class="attendee-list">
                    <li>{{time.registrant.first_name}} {{time.registrant.last_name}}</li>
                    <li ng-repeat="guest in time.registrant.guests">
                        <span style="opacity:.25;font-size:16px;">&rdsh;</span>{{guest.first_name}} {{guest.last_name}}
                    </li>
                </ul>
                <a ng-click="register(time)" class="btn">Confirm Reservation!</a>
                <br />
                <a onClick="window.location.reload()" class="cancel">Start Over</a>
            </div>
        
        </div>
        
        
        
        
        
        
    </div>
        
        
        
</div>
<div ng-show="admin"  class="sharebox" style="width:40%;background:rgba(255,255,255,.1); margin:50px auto;font-size:12px;padding:30px;border:1px dashed rgba(255,255,255,.4);border-radius:3px;text-align:center;">
	<small>To share this event copy and paste this url into social media posts, emails, etc.</small>
    <br />
    <h2>http://signuplounge.com/event/{{event.url_key}}</h2>
</div>
