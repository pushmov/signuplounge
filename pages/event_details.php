<div class="container" ng-init="getEventDetails('<?=$i?>')">
	<div class="title-block">
    	<h1>{{event.name}}</h1>
        <h2>{{event.date}}</h2>
        
        <a class="btn" ng-href="event/{{event.url_key}}">See Public Event</a>
        
        {{event}}
        {{event_times}}
    </div>
</div>