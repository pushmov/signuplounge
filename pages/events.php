<div class="container">
	<div class="title-block">
    	<h1>Events.</h1>
        <h2>Create your event here!</h2>
        <a ng-show="!showEventForm" class="btn large" ng-click="initEventForm()">+ Create an Event</a>
    </div>
    
    <form ng-show="showEventForm" ng-submit="createEvent(newEvent)">
    	<div class="nice">
        	<label>Event Name</label>
        	<input ng-model="newEvent.name">
        	<label>Date <span>(mm/dd/yyyy)</span></label>
        	<input ng-model="newEvent.eventDate">
        	<label>Location</label>
        	<input ng-model="newEvent.location">
        	<label>Description</label>
        	<textarea ng-model="newEvent.description"></textarea>
        </div>
        <div ng-repeat="times in newEvent.eventTimes">
            <hr>
    		<div class="nice">
                <label>Start Time <span>(7am, 2:15pm, 10pm)</span></label>
                <input ng-model="times.startTime">
                <label>End Time <span>(7am, 2:15pm, 10pm)</span></label>
                <input ng-model="times.endTime">
                <label>Max Capacity</label>
                <input ng-model="times.capacity">
        	</div>
            <label><input type="checkbox" ng-model="times.waitlist" ng-true-value="1" ng-false-value="0"> Allow Waitlist</label>
            <a ng-if="$index!=0" class="btn-b right" ng-click="removeEventTime($index)">- Remove time slot.</a>
            <div class="clr"></div>
        </div>
        <a class="btn-b" ng-click="addEventTime()">+ Add another time slot.</a>
        <div class="clr"></div>
        <input type="submit" class="btn" value="Save this Event">
        <a class="cancel" ng-click="cancelEventForm()">Cancel</a>
    </form>
    
    
</div>

<div ng-show="!showEventForm" ng-init="getEvents()" class="event-list">
	<div ng-repeat="event in events" ng-init="getEventsTimes(event, event.event_id)" class="event-preview">
		<a href="event/{{event.url_key}}">
        	{{event.name}} <span class="right">{{event.date}}</span>
            <!--
            <br />
            <small>{{event.description}}</small>
            <div ng-repeat="time in event.times">
            {{time.start_time}}
	        <span ng-show="time.capacity" class="right">Capacity: {{time.capacity}}</span>
            </div>
            -->
        </a>
	</div>
</div>