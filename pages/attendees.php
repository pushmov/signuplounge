<div class="container">
	<div class="title-block">
    	<h1>Attendees.</h1>
        <h2>Manage new and existing attendees here.</h2>
        <a ng-show="!showEventForm" class="btn large" ng-click="showEventForm=true">+ Create a new attendee.</a>
    </div>
</div>