<div class="container">
    <div class="title-block">
    	<h1>Create an account.</h1>
        <h2>We'll help make organizing easy!</h2>
    </div>
    <form name="userForm" ng-submit="signup(newUser)" class="userForm">
        <div class="nice">
            <label>Email</label>
            <input name="email" ng-model="newUser.email">
            <label>Password</label>
            <input type="password" name="password" ng-model="newUser.password">
        </div>
        <input class="btn" type="submit" value="Sign Up &raquo;">
    </form>
</div>